# PlantUML Previewer for NeoVim
This a simple extention that reads a portion of you current document, detects the
tokens `@startuml` and `@enduml` and process the portion of text between them
throw [PlantUML](https://plantuml.com) JAR file.

![Preview](https://imgur.com/eAnEzAx.gif)

## Requirements
This plugin requires that you have `java` installed in your system and to download
the PlantUML JAR file (visit https://github.com/plantuml/plantuml/releases to get
the one you need).

## Installation
Using [packer.nvim](https://github.com/wbthomason/packer.nvim)
```lua
use({
    "https://gitlab.com/daemonraco/plantuml-preview.nvim",
    requires = { "MunifTanjim/nui.nvim" },
})
```

## Setup
This is a full configuration of this plugin. Read carefully each parameter, some
are not required and you may not need them.
```lua
require("plantuml-preview").setup({
    plant_uml = "/full/path/to/plantuml.jar", -- this is a required configuration.
    tmp_directory = "/my/temp/directory", -- by default it assumes '/tmp'.
})
```

## Keymaps
Here is an example on how to configure a keymap for this plugin, although your
free to choose whatever combination suits your configuration:
```lua
vim.keymap.set("n", "<C-o>p", ":PlantUMLPreview<CR>")
```
