local Popup = require("nui.popup")
local event = require("nui.utils.autocmd").event

local loading = nil

local function hide_loading()
    if loading then
        loading:unmount()
        loading = nil
    end
end

local function show_error(message)
    hide_loading()

    local popup = Popup({
        enter = true,
        focusable = true,
        border = {
            padding = {
                top = 1,
                bottom = 1,
                left = 1,
                right = 1,
            },
            style = "rounded",
            text = {
                top = " PlantUML Preview: Error ",
                top_align = "center",
                bottom = " Use q, esc or Enter to close this window ",
                bottom_align = "right",
            },
        },
        position = {
            row = "30%",
            col = "50%",
        },
        size = {
            width = "50%",
            height = 1,
        },
    })

    local function leave()
        popup:unmount()
    end
    popup:map("n", "<esc>", function(bufnr) leave() end, { noremap = true })
    popup:map("n", "<cr>", function(bufnr) leave() end, { noremap = true })
    popup:map("n", "q", function(bufnr) leave() end, { noremap = true })
    popup:map("n", "Q", function(bufnr) leave() end, { noremap = true })
    popup:mount()

    popup:on(event.BufLeave, leave)

    vim.api.nvim_buf_set_lines(popup.bufnr, 0, 1, false, message)
end

local function show_loading()
    if not loading then
        loading = Popup({
            enter = true,
            focusable = true,
            border = {
                padding = {
                    top = 0,
                    bottom = 0,
                    left = 1,
                    right = 1,
                },
                style = "rounded",
                text = {
                    top = " PlantUML Preview: Loading... ",
                    top_align = "center",
                },
            },
            position = {
                row = "30%",
                col = "50%",
            },
            size = {
                width = "30%",
                height = 1,
            },
        })

        loading:mount()
        vim.api.nvim_buf_set_lines(loading.bufnr, 0, 1, false, { "Processing script..." })
    end
end

local function show_preview(lines)
    hide_loading()

    local popup = Popup({
        enter = true,
        focusable = true,
        border = {
            padding = {
                top = 2,
                bottom = 2,
                left = 2,
                right = 2,
            },
            style = "rounded",
            text = {
                top = " PlantUML Preview ",
                top_align = "center",
                bottom = " Use q, esc to close this window ",
                bottom_align = "right",
            },
        },
        position = "50%",
        size = {
            width = "90%",
            height = "90%",
        },
    })

    local function leave()
        popup:unmount()
    end
    popup:map("n", "<esc>", function(bufnr) leave() end, { noremap = true })
    popup:map("n", "q", function(bufnr) leave() end, { noremap = true })
    popup:map("n", "Q", function(bufnr) leave() end, { noremap = true })
    popup:mount()

    popup:on(event.BufLeave, function()
        leave()
    end)

    vim.api.nvim_buf_set_lines(popup.bufnr, 0, 1, false, lines)
end

return {
    hide_loading = hide_loading,
    show_error = show_error,
    show_loading = show_loading,
    show_preview = show_preview,
}
