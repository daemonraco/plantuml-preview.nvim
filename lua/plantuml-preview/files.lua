local configuration = require('plantuml-preview.config').configuration
local popup_utils = require('plantuml-preview.popups')

local function analyzeInput()
    local cmd = string.format("cat '%s'|java -jar '%s' -txt -pipe > '%s'", configuration.input_file, configuration.plant_uml, configuration.output_file)
    local status = vim.fn.system(cmd)
    if not status == 0 then
        popup_utils.show_error({ "Unable to run PlantUML." })
        return false
    end

    return true
end

local function getOutput()
    local file = io.open(configuration.output_file, "r")
    if file then
        local contents = file:read("*a")
        file:close()

        local lines = {}
        for l in contents:gmatch("[^\n]+") do 
            table.insert(lines, l)
        end
        return lines
    else 
        popup_utils.show_error({ "ERR: unable to read output file '"..configuration.output_file.."'." })
        return nil
    end
end

local function saveInput(text)
    local file = io.open(configuration.input_file, "w")
    if file then
        file:write(text)
        file:close()
    else 
        popup_utils.show_error({ "Unable to write temporary file '"..configuration.input_file.."'." })
        return false
    end

    return true
end

return {
    analyzeInput = analyzeInput,
    getOutput = getOutput,
    saveInput = saveInput,
}
