local configuration = require('plantuml-preview.config').configuration
local core_checks = require('plantuml-preview.checks').core_checks
local popup_utils = require('plantuml-preview.popups')
local files_utils = require('plantuml-preview.files')

local function previewPlant()
    if core_checks() then
        --
        -- Loading current buffer lines.
        local current_buf = vim.api.nvim_get_current_buf()
        local lines = vim.api.nvim_buf_get_lines(current_buf, 0, -1, false)
        --
        -- Getting current position.
        local cursor_pos = vim.api.nvim_win_get_cursor(0)
        local cursor_line = cursor_pos[1]
        --
        -- Guessing the line where the current portion starts.
        local start_line = cursor_line
        while start_line > -1 do
            local found = lines[start_line] and lines[start_line]:find(configuration.end_token, 0, false)
            if found then
                start_line = nil
                break
            end
            local found = lines[start_line] and lines[start_line]:find(configuration.start_token, 0, false)
            if found then
                break
            end

            start_line = start_line - 1
        end
        --
        -- Guessing the line where the current portion ends.
        local end_line = cursor_line
        local max = table.getn(lines)
        while end_line <= max do
            local found = lines[end_line] and lines[end_line]:find(configuration.start_token, 0, false)
            if found then
                end_line = nil
                break
            end
            local found = lines[end_line] and lines[end_line]:find(configuration.end_token, 0, false)
            if found then
                break
            end

            end_line = end_line + 1
        end
        --
        -- Cleaning up wrong values
        if start_line and start_line < 0 then
            start_line = nil
        end
        if end_line and end_line > max then
            end_line = nil
        end
        --
        -- Extracting or prompttin an error.
        if start_line and end_line then
            popup_utils.show_loading()

            local portion = { unpack(lines, start_line, end_line) }
            local text = table.concat(portion, "\n")
            local ok = files_utils.saveInput(text)
            if ok then
                files_utils.analyzeInput()
                if ok then
                    local output = files_utils.getOutput()
                    if output then
                        popup_utils.show_preview(output)
                    end
                end
            else
            end
        else
            popup_utils.show_error({ "No tags found." })
        end
    end
end

local function setup(conf)
    if conf.tmp_directory then
        configuration.input_file = conf.tmp_directory..configuration.input_name
        configuration.output_file = conf.tmp_directory..configuration.output_name
    else
        configuration.input_file = configuration.tmp_directory..configuration.input_name
        configuration.output_file = configuration.tmp_directory..configuration.output_name
    end

    if conf.plant_uml then
        configuration.plant_uml = conf.plant_uml
    else
        popup_utils.show_error({ "PlantUML jar file is not configured." })
    end
end

vim.cmd([[
    command! -nargs=* PlantUMLPreview lua require('plantuml-preview').preview()
]])

return {
    preview = previewPlant,
    setup = setup,
}
