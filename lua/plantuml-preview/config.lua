local configuration = { 
    tmp_directory = "/tmp",
    input_name = "/nvim.plantuml.plg.input.txt",
    output_name = "/nvim.plantuml.plg.output.txt",
    input_file = "/tmp/nvim.plantuml.plg.input.txt",
    output_file = "/tmp/nvim.plantuml.plg.output.txt",
    start_token = "@startuml",
    end_token = "@enduml",
    plant_uml = nil,
}

return {
    configuration = configuration
}
