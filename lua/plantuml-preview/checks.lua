local configuration = require('plantuml-preview.config').configuration
local popup_utils = require('plantuml-preview.popups')

local function core_checks()
    --
    -- Checking if java is accesible.
    if vim.fn.executable('java') == 0 then
        popup_utils.show_error({ "'java' is not available in $PATH." })
        return false
    end
    --
    -- Checking if the PlantUML JAR file is set and accessible.
    if not configuration.plant_uml then
        popup_utils.show_error({ "PlantUML jar file is not set." })
        return false
    end
    local jar = io.open(configuration.plant_uml, "r")
    if jar then
        jar:close()
    else
        popup_utils.show_error({ "PlantUML jar file is not accessible." })
        return false
    end

    -- if not pcall(require, "nui") then
    --     error("PlantUML Preview: Plugin 'MunifTanjim/nui.nvim' is not installed.")
    -- end

    return true
end

return {
    core_checks = core_checks,
}
